# Manyverse Docker

## Why docker?

[Docker](https://www.docker.com/why-docker) is a tool used to manage dependencies, build and deploy any large project all in a single or multiple container(s) / virual environments running inside a host environment i.e. OSX, Linux or Windows. 

Docker is therefore used in the manyverse react native project to manage its dependencies and make it very easy for developers to be able to build and deploy the manyverse app to their android devices without having to go through the hassle of installing and setting up the required react-native, node.js, android-sdk & android-ndk dependencies.

## Using Docker

###  Mac OSX & Windows

The manyverse docker setup in Mac OSX & Windows requires a few extra steps to enable usb port passthrough/usb port sharing from the host environment (OSX/Windows) to the docker container, because this isn't supported natively by either [Docker for Mac](https://github.com/docker/for-mac/issues/900) or [Docker for Windows](https://forums.docker.com/t/docker-for-windows-usb-support/38693/4). 

This usb passthrough/sharing feature is very important as it will enable the developer to easily build & install the manyverse apk to their respective android device via a usb cable connected to their laptop/desktop. 

To therefore setup docker for manyverse in OSX or Windows, please do the following :-

- Install the appropriate docker build for your environment i.e. for OSX use [Docker for Mac](https://docs.docker.com/docker-for-mac/install/) and for Windows use [Docker for Windows](https://docs.docker.com/docker-for-windows/install/)
- Once the docker build is successfully installed, then install [VirtualBox 6.0.6 or higher](https://www.virtualbox.org/wiki/Downloads) and then afterwards install the respective VirtualBox extension pack for the VirtualBox version you have installed.
- Create a docker machine by running the command `docker-machine create --driver virtualbox manyverse-docker` in your terminal/cmd.
	-	Please note the name `manyverse-docker` in the command above can be anything you choose to call the docker machine 
- Then run the command `docker-machine ls` to make sure the `docker-machine` created above is running.
- If the docker machine is running without any errors i.e. the STATE is Running, then stop it via the command `docker-machine stop` and run the command `vboxmanage modifyvm default --usbxhci on` to enable usb passthrough to the `docker-machine`.
- Afterwards check if the host machine's usb ports have been shared successfully to the docker machine via the command `vboxmanage list usbhost` and if nothing is listed, please make sure the correct VirtualBox extension has been installed for the version of VirtualBox you are using in your host machine.
- If the usb ports have been successfully shared to the docker machine, then open the VirtualBox app in your system, click on the docker machine you created above, navigate to `Settings -> Ports - > USB` 
	- Select the option `Enable USB controller`
	- Connect your android device to your host machine and make sure [USB debugging](https://www.embarcadero.com/starthere/xe5/mobdevsetup/android/en/enabling_usb_debugging_on_an_android_device.html) is enabled.
	- Finally add the android device under `USB Filters` and make sure the device is checked.
- Also add the manyverse project as a shared folder in VirtualBox by doing the following
	- First navigate to `Settings -> Shared Folders` and select your manyverse project in the host machine.
	- Then afterwards make sure `Auto-mount` is checked and `Read-only` is unchecked and then click on the ok button

- Navigate to `Settings -> Display` and adjust the scale factor to atleast 200%, so that you get a bigger window for the docker machine when its running.

After doing the above the docker machine will now be ready to use.

Finally to now build the project, do the following:-
- Disconnect your android device from your host machine/laptop/desktop, THIS IS A VERY IMPORTANT STEP OTHERWISE YOU WON'T BE ABLE TO DEPLOY TO YOUR DEVICE.
- Also make sure adb usb mode is enabled in your host by running the command `adb usb` in your host machine.
- Then boot up the docker-machine by clicking on the `start` button in VirtualBox or by running the command `docker-machine ssh manyverse-docker` where `manyverse-docker` is replaced by the name of your docker machine.
- Once the docker machine finishes booting up to the console, tap the the return key on your keyboard until you get to a GREEN prompt which shows `docker@manyverse-docker` where `manyverse-docker` is replaced by the name of your docker machine.
- If this is the first time you are running the  project in docker machine, run the command `docker build -t manyverse .` to build the docker container and then after this command is done, run the shell script `./docker/run.sh` to connect the docker container.
- If the docker image has already been built, simply run the shell script `./docker/run.sh`  to connect to the docker container.

### Linux Distros

Since the linux kernel already exposes the usb ports in the host machine to Docker for Linux, you don't have to go through the above method of setting up a docker machine for any of the linux distros. Instead, build the docker container via a terminal/cli via the command `docker build -t manyverse .` if its the first time building the manyverse project and then simply run the script `./docker/run.sh` to connect the docker container and build the manyverse app as shown below.

#### Building and Deploying the Manyverse app/apk

Once you are successfully connected to the container, connect your android device via a usb cable to your host machine and do the following :-

- Make sure the usb connected android device is visible to the container by running the command `adb devices` 
	- PS, if a popup appears on your android device asking you to authenticate the fingerprint, tap on ok.
- Finally run the command(s) `npm run full-clean && npm i && npm run build-android-debug` to build and install the app to your device.

