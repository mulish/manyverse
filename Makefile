IMAGE ?= manyverse
GIT_TAG ?= $(shell git log --oneline | head -n1 | awk '{print $$1}')
TAG ?= latest
DOCKER_ARGS ?= 
DCMD ?= docker run --rm $(DOCKER_ARGS) -u $$(id -u):$$(id -g) -v $$(pwd):/manyverse -w /manyverse $(IMAGE):$(TAG)
ROOT_DCMD ?= docker run --rm -e HOME=/manyverse -v $$(pwd):/manyverse -w /manyverse $(IMAGE):$(TAG)
# .PHONY: clean
# clean:

.PHONY: docker
docker:
	docker build -t $(IMAGE) .
	docker tag $(IMAGE) $(IMAGE):$(GIT_TAG) #&& docker push $(IMAGE):$(GIT_TAG)
	docker tag $(IMAGE) $(IMAGE):latest # && docker push $(IMAGE):latest

.PHONY: interact
interact: DOCKER_ARGS=-it
interact:
	$(DCMD) bash

.PHONY: interact-root
interact-root:
	docker run --rm -it -v $$(pwd):/manyverse -w / $(IMAGE):$(TAG) bash