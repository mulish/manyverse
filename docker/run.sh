#!/bin/bash
docker run -t -i --privileged -v /dev/bus/usb:/dev/bus/usb -v /manyverse:/manyverse  -v /manyverse/node_modules -v /manyverse/nodejs-assets -v /manyverse/android/build utunga/ssb_build /bin/bash

#NB mapping a 'data volume' such as by `-v /manyverse/android/build` isolates that part of the file system from the volumes shared with host - which is necessary for sym links to work correctly 
